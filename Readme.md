----------
## Règles du jeux 

- Objectif du jeux :
  - Le premier joueur ayant 10 mots gagne la partie

- Déroulement du jeux :
  - Chacun des joueurs tire une lettre aléatoire d'un sac, et les mettent au milieu dans le pot commun
  - Le joueur qui a tiré la lettre la plus petite lettre dans l'alphabet commence
  - Chaque fois que c'est le début du tour d'un joueur il tire deux lettres aléatoires qu'il rajoute au pot commun
  - Chaque fois qu'un joueur fait un mot il tire une lettre aléatoire qu'il rajoute au pot commun
  - Quand le joueur ne trouve plus de mot il passe et le joueur suivant commence son tour (par tirer 2 lettres qu'il rajoute au pot commun)

- Comment faire un mot ?
  - En utilisant uniquement les lettres du pot commun
  - En prenant un mot de ces adversaires (toutes les lettres du mot) et en lui rajoutant des lettres du pot commun
  - En rallongeant un de ces mots avec des lettres du pot commun ou en utilisant un autre mot (toutes les lettres)
  - Attention, seul les noms communs sont autorisés

- Pour faciliter :
  - les lettres possibles sont uniquement les 26 de l'alphabet (ex : é <-> e)
  - les mots composés sont considérés comme deux mots

- Pour les plus avancés :
  - Le cas des anagrammes :
    - On peut voler un mot en faisant un anagramme uniquement si il n'a pas déjà été fait. Bien entendu, faire un anagramme permet de tirer une nouvelle lettre.

---
## Application 

### Git
```shell
$ git clone https://gitlab.com/Audrine/LetterGame.git
$ cd LetterGame/LetterGame
$ mvn clean package
$ java -jar target/LetterGame-0.1-jar-with-dependencies.jar
```

## Téléchargement
Vous pouvez directement télécharger l'application [ici](http://lettergame.esy.es/bin/letter-game-0.1.jar)

Puis exécuter avec cette commande :

```shell
$ java -jar letter-game-0.1.jar
```
---
## Architecture du projet 
### Objectif des différents packages 
* `Package Dictionary` : le package dictionary à pour objectif d'accéder au fichier dictionnaire pour vérifier que le mot proposé est bien dans celui ci. [lien packgage](LetterGame/src/main/java/fr/esiea/audrine/lapin/dictionary)

* `Package  Player` : Le package player à pour objectif de décrire tous ce qu'un joueur est, possède et peut faire dans le jeu. [lien packgage](LetterGame/src/main/java/fr/esiea/audrine/lapin/player)

* `Package  Command` : Le package command à pour objectif de décrire toutes les différentes fonctionnalités que le joueur peut utiliser pour jouer . [lien packgage](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/command)

* `Package Util` : Le package util a pour objectif de fournir des classes utilitaires au bon fonctionnement du projet.  [lien packgage](LetterGame/src/main/java/fr/esiea/audrine/lapin/util) 

---
## Test unitaire 

#### Package test 

Le package test à pour objectif de tester les méthodes critique du projet. Son architecture ressemble au package 'fr.esiea.audrine.lapin' pour une meilleur lisibilité lors de la revu de code ou d'erreurs détectés.

#### Mockito 

Pour une plus grande indépendence entre les classes et éviter l'attente d'entrées utilisteur lors de l'exécution des tests, j'ai décidé d'utiliser mockito pour simuler certaine classes.

---
## Evaluation du code 

#### Outils d'audit 
* `FindBug` :  FindBug est utilisé dans le code pour la détection de bug dans le code
[lien vers FindBug](http://findbugs.sourceforge.net/)

* `PMD` :  PMD et un plugin  utilisé pour auditer la qualité du code. 
[lien vers PMD](https://pmd.github.io/)

* `Cobertura` : Cobertura et le plugin maven utilisé pour calculer le pourcentage de code que couvre les tests unitaires.
[lien vers Cobertura](http://cobertura.github.io/cobertura/)

#### Les Rapports 
Pour générer les rapports : 
```shell
$ mvn clean install -B cobertura:cobertura site # si faild lancer une deuxième fois
```
Les rapports sont aussi générés de façon automatique grâce au système qu'offre gitlab

*  `Artifacts gitlab` à télécharger depuis le [Pipeline](https://gitlab.com/Audrine/LetterGame/pipelines) les rapports de type XML et HTML 

* `Site internet`  où est déployée la version HTML des différents rapports sont mis sur un hébergeur web. [Rapport LetterGame](http://lettergame.esy.es/)




 
