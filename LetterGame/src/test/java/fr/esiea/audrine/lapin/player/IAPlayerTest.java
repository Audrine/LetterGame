package fr.esiea.audrine.lapin.player;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

import fr.esiea.audrine.lapin.dictionary.IDictionary;
import fr.esiea.audrine.lapin.util.Validator;

public class IAPlayerTest {
	
	static final String INVALIDE_COMMUN_POT = "invalide";
	static final String VALIDE_COMMUN_POT = "valide"; 
	IDictionary dictionaryMock = Mockito.mock(IDictionary.class);
	Validator validator;
	IAPlayer iaPlayer; 
	
	@Before
	public void setup(){
		when(dictionaryMock.bruteForceSearch(INVALIDE_COMMUN_POT)).thenReturn(null);
		when(dictionaryMock.bruteForceSearch(VALIDE_COMMUN_POT)).thenReturn("my_word");
		validator = new Validator(dictionaryMock);
		iaPlayer = new IAPlayer("ia_0", validator); 
	}
	
	@Test
	public void testProposedWord(){
		iaPlayer.setCommunPot(INVALIDE_COMMUN_POT);
		assertFalse(iaPlayer.proposeWord());
		iaPlayer.setCommunPot(VALIDE_COMMUN_POT);
		assertTrue(iaPlayer.proposeWord());
	}
	
	@Test
	public void testExtendWord(){
		assertFalse(iaPlayer.extendWord());
	}
	
	@Test
	public void testStealOpponentWord(){
		assertFalse(iaPlayer.stealOpponentWord());
	}
	
	@Test
	public void testPlaye(){
		ArrayList<Player> listPlayer = new ArrayList<Player>();
		listPlayer.add(iaPlayer); 
		iaPlayer.setListPlayer(listPlayer);
		iaPlayer.getListWord().clear();
		iaPlayer.setCommunPot(VALIDE_COMMUN_POT);
		iaPlayer.playe();
		assertEquals(1, iaPlayer.getListWord().size());
	}
}
