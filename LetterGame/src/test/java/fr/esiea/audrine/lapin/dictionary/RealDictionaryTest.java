package fr.esiea.audrine.lapin.dictionary;

import org.junit.Test;
import static org.junit.Assert.*;
public class RealDictionaryTest {
	
	@Test
	public void testSansAccents(){
		String actual = RealDictionary.sansAccents("décoration");
		String expected = "decoration"; 
		assertEquals(expected, actual);
	}

}
