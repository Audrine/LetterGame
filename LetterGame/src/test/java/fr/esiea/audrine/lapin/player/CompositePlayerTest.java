package fr.esiea.audrine.lapin.player;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import java.util.ArrayList;


public class CompositePlayerTest {
	
	CompositePlayer compositePlayer; 
	
	@Mock
	private ArrayList<String> mockArrayList;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		
 	}
	
	@Test
	public void testPlaye(){
		compositePlayer = new CompositePlayer(); 
		Player player = Mockito.mock(Player.class, Mockito.RETURNS_MOCKS);
		when(mockArrayList.size()).thenReturn(10); 
		when(player.getListWord()).thenReturn(mockArrayList); 
		compositePlayer.addPlayer(player);
		compositePlayer.playe();
	}

}
