package fr.esiea.audrine.lapin.player.command;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import fr.esiea.audrine.lapin.player.Player;

public class CommandTest {
	
	Player player; 
	Invoker invoker; 
	
	@Before
	public void setup(){
		player = Mockito.mock(Player.class); 
		when(player.extendWord()).thenReturn(true);
		when(player.stealOpponentWord()).thenReturn(true);
		when(player.proposeWord()).thenReturn(true);
		invoker = new Invoker(player);
		
	}
	
	@Test
	public void testCommands(){
		assertTrue(invoker.getCommandPlayer("proposed").execute());
		assertTrue(invoker.getCommandPlayer("steal_word").execute()); 
		assertTrue(invoker.getCommandPlayer("extend_word").execute()); 
	}

}
