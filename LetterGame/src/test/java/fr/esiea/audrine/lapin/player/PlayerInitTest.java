package fr.esiea.audrine.lapin.player;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import fr.esiea.audrine.lapin.util.ScanObject;

public class PlayerInitTest {
	
	PlayerInit playerInit = new PlayerInit(); 
	ScanObject scanObject = Mockito.mock(ScanObject.class); 
	
	@Before
	public void setup(){
		playerInit.setScanObject(scanObject);
		when(scanObject.scan("Entrer le nombre de joueur")).thenReturn("1000").thenReturn("2");
		when(scanObject.scan("Entrer le nombre de joueur virtuel")).thenReturn("1000").thenReturn("2");
		when(scanObject.scan("Entrer le nom du joueur")).thenReturn("oui").thenReturn("ouiè").thenReturn("oui");
	}
	
	@Test
	public void testSetSimplePlayerNumber(){
		playerInit.setSimplePlayerNumber(false);
		assertEquals(2, playerInit.getNbSimplePlayer());
	}
	@Test
	public void testSetIAPlayerNumber(){
		playerInit.setIAPlayerNumber(false);
		assertEquals(2, playerInit.getNbIAPlayer());
	}
	
	@Test
	public void testSetListSimplePlayerName(){
		playerInit.setNbSimplePlayer(2);
		playerInit.setListSimplePlayerName();
		assertFalse(playerInit.getMapNamePlayer().isEmpty());
		int size = playerInit.getMapNamePlayer().size();
		assertEquals(2, size);
	}
	
	@Test
	public void testSetListIAPlayerName(){
		playerInit.setNbIAPlayer(2);
		playerInit.setListIAPlayerName();
		assertFalse(playerInit.getMapNamePlayer().isEmpty());
		int size = playerInit.getMapNamePlayer().size();
		assertEquals(2, size);
	}
	
	@Test
	public void testSortHasmap(){
		playerInit.getMapNamePlayer().put('a', "test");
		playerInit.sortHasmap();
		assertNotNull(playerInit.compositePlayer);
		assertNotNull(playerInit.startedCommunpot);
	}
	
}
