package fr.esiea.audrine.lapin.util;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import fr.esiea.audrine.lapin.dictionary.IDictionary;
public class ValidatorTest {
	Validator serviceValidator;
	IDictionary dictionary;
	
	@Before
	public void SetUp(){
		this.dictionary = Mockito.mock(IDictionary.class);
		this.serviceValidator = new Validator(dictionary);
	}
	@Test
	public void testStealWordValidate(){
		when(dictionary.isWord("pire")).thenReturn(true); 
		assertTrue(serviceValidator.stealWordValidate("pire", "pie", "hr"));
	}
	
	@Test
	public void testExtendWordValidate(){
		when(dictionary.isWord("voitures")).thenReturn(true);
		when(dictionary.isWord("f")).thenReturn(true);
		when(dictionary.isWord("vomissement")).thenReturn(true); 
		assertTrue(serviceValidator.extendWordValidate("voitures", "voiture", null, "soui"));
		assertFalse(serviceValidator.extendWordValidate("fou", "f", null, "oubsdjbvdhsvb"));
		assertTrue(serviceValidator.extendWordValidate("vomissement", "vomis", "sement", null));
		assertFalse(serviceValidator.extendWordValidate("vomissement", "vomis", "men", null));
	}
	
	@Test 
	public void testValideWord(){
		when(dictionary.isWord("bien")).thenReturn(true);
		when(dictionary.isWord("clic")).thenReturn(true); 
		assertTrue(serviceValidator.validWord("bien", "oknhbtendhdbclic"));
		assertFalse(serviceValidator.validWord("clic", "knhbndhdb"));
	}

}
