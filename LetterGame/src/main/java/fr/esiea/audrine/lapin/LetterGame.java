package fr.esiea.audrine.lapin;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import fr.esiea.audrine.lapin.player.CompositePlayer;
import fr.esiea.audrine.lapin.player.PlayerInit;

public class LetterGame {


	public LetterGame() {}
	
	@SuppressWarnings("resource")
	public void run(){
		Scanner scan = new Scanner(System.in, StandardCharsets.UTF_8.name()); 
		PlayerInit playerInit = new PlayerInit();
		playerInit.run();
		System.out.println("Appuyer sur entrer pour commencer le jeu ...");
		scan.nextLine(); 
		CompositePlayer players = playerInit.compositePlayer;
		players.setCommunPot(playerInit.startedCommunpot);
		players.playe();
	}
}
