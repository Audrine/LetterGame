package fr.esiea.audrine.lapin.player.command;

import fr.esiea.audrine.lapin.player.Player;

public abstract class CommandPlayer implements ICommandPlayer{
	Player player; 
	
	public CommandPlayer(Player player) {
		this.player = player; 
	}
}
