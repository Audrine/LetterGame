package fr.esiea.audrine.lapin.util;

import java.util.Scanner;

	public class ScanObject {
	
	Scanner scan;
	
	public ScanObject() {
		
	}
	public ScanObject(Scanner scan){
		this.scan = scan;
	}
	
	public String scan(String sentence){
		System.out.println(sentence);
		System.out.print(">> ");
		return scan.nextLine(); 
	}
	
	public void clearBufferScan(){
		scan.nextLine(); 
	}

}
