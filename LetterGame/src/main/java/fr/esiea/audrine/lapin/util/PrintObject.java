package fr.esiea.audrine.lapin.util;

import java.util.ArrayList;
import java.util.List;

import de.vandermeer.skb.asciitable.AsciiTable;
import de.vandermeer.skb.asciitable.StandardTableThemes;
import de.vandermeer.skb.asciitable.TableOptions;
import fr.esiea.audrine.lapin.player.Player;

public class PrintObject {
	public static void printTitle(){
		System.out.println("__     ____ ______ ______  ____ ____       ___   ___  ___  ___  ____");
		System.out.println("||    ||    | || | | || | ||    || \\\\     // \\\\ // \\\\ ||\\\\//|| ||   ");
		System.out.println("||    ||==    ||     ||   ||==  ||_//    (( ___ ||=|| || \\/ || ||== ");
		System.out.println("||__| ||___   ||     ||   ||___ || \\\\     \\\\_|| || || ||    || ||___");
		System.out.println("");
		System.out.println("");
	}
	
	public static void printPrincipaleMenu(){
		PrintObject.printTitle();
		System.out.println("1. Jouer");
		System.out.println("");
		System.out.println("2. les règles");
		                                                                     
	}
	public static void printTableresult(ArrayList<Player>  listPlayers, int nbPlayer){
		String[] [] table = new String[11][nbPlayer];
		int i = 0;
		int y = 0;
		
		for(Player player : listPlayers){
			table[y][i] = Integer.toString(i+1) + ":" + player.getName();
			List<String> listWord = player.getListWord();
			for(int index=0; index < 10 ; index++ ){
				y++;
				if( listWord.size() <= index){
					table[y][i] = " ";
				}
				else{
					table[y][i] = Integer.toString(index+1) + '.' + listWord.get(index);
				}
			}
			y= 0; 
			i++; 
		}
		AsciiTable at=AsciiTable.newTable(nbPlayer, 90);
		y= 0;
		while(y < 11){
			 at.addRow((Object[]) table[y]);
			 y++;
		}	
		System.out.println(at.render(new TableOptions().setRenderTheme(StandardTableThemes.LIGHT)));
				
	}
	
	public final static void clearConsole()
	{
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}
	public static void printCommunPot(CharSequence letterssPot){
		AsciiTable at=AsciiTable.newTable(1, 50);
		at.addRow("Pot Commun");
		//at.addRow(serviceCommunPot.getCommunPot().getLettersPot());
		at.addRow(letterssPot);
		System.out.println(at.render(new TableOptions().setRenderTheme(StandardTableThemes.LIGHT)));
	}
	
	public static void printHedaer(ArrayList<Player> listPlayers, CharSequence letterssPot){
		int nbPlayer = listPlayers.size();
		clearConsole();
        printTitle();
        printTableresult(listPlayers, nbPlayer);
        PrintObject.printCommunPot(letterssPot);
	}


}
