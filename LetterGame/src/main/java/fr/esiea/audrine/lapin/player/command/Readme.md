## Package Commande
#### Responsabilité dans le code 
Ce package a été créé pour générer les différente fonctionnalités du joueur.

#### Patron de conception : Commande 
Pour l'architecture de ce package j'ai décidé d'utiliser le partons de conception [Commande](https://fr.wikibooks.org/wiki/Patrons_de_conception/Commande)

Avantage :  Cela nous évite d'écrire des switchs qui peuvent être longs et incompréhensibles, de plus il nous  permet d'ajouter aisément de nouvelles fonctionalités que le joueur pourra utilisé 

