package fr.esiea.audrine.lapin.dictionary;

public interface IDictionary {
	public boolean isWord(String word);
	public String bruteForceSearch(CharSequence communPot);
}
