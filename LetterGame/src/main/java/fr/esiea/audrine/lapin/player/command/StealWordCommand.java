package fr.esiea.audrine.lapin.player.command;
import fr.esiea.audrine.lapin.player.Player;

public class StealWordCommand extends CommandPlayer{

	public StealWordCommand(Player player) {
		super(player);
	}

	@Override
	public boolean execute() {
		return player.stealOpponentWord();
	}


}
