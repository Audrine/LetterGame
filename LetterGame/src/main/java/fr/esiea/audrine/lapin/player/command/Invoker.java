package fr.esiea.audrine.lapin.player.command;

import java.util.HashMap;

import fr.esiea.audrine.lapin.player.Player;


public class Invoker {
	
	Player player; 
	HashMap<String, ICommandPlayer> mapCommandPlayer = new HashMap<String, ICommandPlayer>();
	
	
	public Invoker(Player player) {
		this.player = player;
		setCommandPlayer();
	}
	public void addCommand(String key, ICommandPlayer iCommandPlayer){
		mapCommandPlayer.put(key, iCommandPlayer);
	}
	
	public void setCommandPlayer(){
		addCommand("proposed", new ProposedWordCommand(player));
		addCommand("steal_word", new StealWordCommand(player));
		addCommand("extend_word", new ExtendWordCommand(player));
	}
	
	public ICommandPlayer getCommandPlayer(String key){
		return mapCommandPlayer.get(key); 
	}
	
	

}
