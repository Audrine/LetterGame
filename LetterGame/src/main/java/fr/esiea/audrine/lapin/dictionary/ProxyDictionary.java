package fr.esiea.audrine.lapin.dictionary;

public class ProxyDictionary implements IDictionary{
	
	String dicoFile; 
	IDictionary iDictionary;
	public ProxyDictionary(String dicoFile) {
		this.dicoFile = dicoFile; 
	}
	public void setRealDictionary(){
		if(iDictionary == null){
			iDictionary = new RealDictionary(dicoFile); 
		}
	}
	@Override
	public boolean isWord(String word) {
		setRealDictionary();
		return iDictionary.isWord(word);
	}
	@Override
	public String bruteForceSearch(CharSequence communPot) {
		setRealDictionary();
		return iDictionary.bruteForceSearch(communPot);
	}
}
