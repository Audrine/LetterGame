package fr.esiea.audrine.lapin.util;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import fr.esiea.audrine.lapin.dictionary.IDictionary;


public class Validator {
	
	IDictionary dictionary; 
	
	public Validator(IDictionary dictionary) {
		this.dictionary = dictionary;
	}
	public boolean validWord(String word, CharSequence communPot){
		return StringUtils.containsAny(communPot, word) && dictionary.isWord(word);
	}
	public boolean stealWordValidate(String word,String opponentWord,CharSequence communPot ) {
		String prefix = word; // manipulation du word
		if(!StringUtils.containsAny(word, opponentWord)){
			return false;
		}
	    for (int i = 0; i < opponentWord.length(); i++) {
	    	
	    	prefix = prefix.replaceFirst(""+opponentWord.charAt(i)+"", "");
	    }
	    if(word.length() - prefix.length() != opponentWord.length() ){
	    	return false; 
	    }
		return StringUtils.containsAny(communPot, prefix) && dictionary.isWord(word);
	} 
	public boolean extendWordValidate(String word, String ownWord, String opponentWord, CharSequence communPot){
		if(!StringUtils.startsWith(word , ownWord)) // ne commence pas par son propre mot 
			return false;
		
		String prefix = word.replace(ownWord, "");
		if(opponentWord != null){
			return (StringUtils.containsAny(prefix, opponentWord) & prefix.length() == opponentWord.length()) & dictionary.isWord(word);
		}
		else if (communPot != null){
			return StringUtils.containsAny(communPot, prefix) && dictionary.isWord(word);
		}
		return false; 
	}
	
	public static boolean isNumber(String stringNumber){
		return Pattern.matches("[0-9]+", stringNumber); 
	}
	public IDictionary getDictionary() {
		return dictionary;
	}

}
