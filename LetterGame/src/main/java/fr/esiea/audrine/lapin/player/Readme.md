## Package Joueur 
#### Responsabilité dans le code 
Les classes présentes dans ce package ont pour but de décrire les joueurs et leurs actions.

#### Patron de conception : Stratégie 
Pour la description des différents types de joueurs j'ai utilisé le patron [Stratégie](https://fr.wikibooks.org/wiki/Patrons_de_conception/Strat%C3%A9gie).

Avantage : j'ai pu créer la classe [`SimplePlayer`](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/SimplePlayer.java) en tant que fille de la classe [Player](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/Player.java), et ensuite créer la classe [IAPlayer](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/IAPlayer.java) avec la même mère.

#### Patron de conception : Factory

Pour la création des joueurs j'ai choisi d'utiliser le patrons de conception Factory.

Avantage : j'ai pu construire les différents objets Player avec le même object [Validator](LetterGame/src/main/java/fr/esiea/audrine/lapin/util/Validator.java). Ce qui m'a permis de respecter le patron de conception Virtual Proxy décrit dans le package [Dictionary](LetterGame/src/main/java/fr/esiea/audrine/lapin/dictionary/).

#### Patron de conception : Composite 

Pour l'exécution du tour de chaque joueur j'ai décidé d'utiliser le patron de conception [Composite](https://fr.wikibooks.org/wiki/Patrons_de_conception/Objet_composite).

Avantage : Ajout facile de différents types de joueurs dans une classe homogène où des objets du même type s'excutent de la même façon. 


#### Les classes 
* [`IPlayer`](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/IPlayer.java) : interface des joueur avec pour seule méthode `playe` qui représente l'exécution du tour du joueur    
* [`CompositePlayer`](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/CompositePlayer.java): Classe Composite qui implémente IPlayer.
* [`Player`](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/Player.java): Classe abstraite qui définit un joueur en général 
* [`SimplePlayer`](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/SimplePlayer.java) , [``IAPlayer](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/IAPlayer.java) : Les deux types héritié de Player
* [`PlayerFactory`](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/PlayerFactory.java) : Classe de production de joueur
* [`PlayerInit`](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/PlayerInit.java) : Classe d'initialisation des joueurs


