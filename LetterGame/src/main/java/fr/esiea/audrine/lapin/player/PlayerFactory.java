package fr.esiea.audrine.lapin.player;

import fr.esiea.audrine.lapin.dictionary.ProxyDictionary;
import fr.esiea.audrine.lapin.util.Validator;

public class PlayerFactory {
	
	private Validator validator = new Validator(new ProxyDictionary("/dico.txt"));
	
	public Player getPlayer(String type, String name){
		if(type.equals("simple")){
			return new SimplePlayer(name, validator); 
		}else if(type.equals("ia")){
			return new IAPlayer(name, validator); 
		}
		else{
			return null;
		}
	}

}
