package fr.esiea.audrine.lapin.player.command;
import fr.esiea.audrine.lapin.player.Player;

public class ProposedWordCommand extends CommandPlayer{

	public ProposedWordCommand(Player player) {
		super(player);
	}

	@Override
	public boolean execute() {
		return player.proposeWord();
	}

}
