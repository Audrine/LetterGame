package fr.esiea.audrine.lapin.player;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import fr.esiea.audrine.lapin.player.command.Invoker;
import fr.esiea.audrine.lapin.util.PrintObject;
import fr.esiea.audrine.lapin.util.ScanObject;
import fr.esiea.audrine.lapin.util.Validator;

public class SimplePlayer extends Player{
	
	ScanObject scan = new ScanObject(new Scanner(System.in, StandardCharsets.US_ASCII.name()));
	String errorMessageOption;
	String errorMessage; 
	//private final static String ERROR_STEAL_WORD = "Vous ne pouvez pas choisir cette option"; 
	private final static String BAD_NUM_OPTION = "Mauvais numero d'option";
	private final static String INVALID_WORD = "Ce mot est invalide";
	private final static String NOT_A_NUM = "Vous devez entrer un numero";
	private final static String OUT_OF_RANGE_NUM_PLAYER = "Ce joueur n'existe pas";
	private final static String OUT_OF_RANGE_WORD_OPPONENT = "Ce joueur n'a pas autant de mot";
	private final static String OUT_OF_RANGE_WORD_PLAYER = "Vous n'avez pas autant de mot";
	public SimplePlayer(String name, Validator validator) {
		super(name, validator);
	}
	
	
	public String choseOpponentWord(){
		String stringNumber = scan.scan("Choisir un de vos adverssaires en fonction de son numero :");
        if(stringNumber.equals("*")){
        	return stringNumber; 
        }
        else if(!Validator.isNumber(stringNumber)){
        	errorMessage = NOT_A_NUM; 
        	return null; 
        }else if(Integer.parseInt(stringNumber) <= 0 || getNbPlayer() < Integer.parseInt(stringNumber)){
        	errorMessage = OUT_OF_RANGE_NUM_PLAYER; 
        	return null;
        }
        int indexPlayer = Integer.parseInt(stringNumber); 
        Player opponentPlayer = listPlayer.get(indexPlayer-1);
        stringNumber =  scan.scan("Choisir un mot de '"+ opponentPlayer.getName() + "' en fonction de son numéro :");
        if(stringNumber.equals("*")){
        	return stringNumber;
        }else if(!Validator.isNumber(stringNumber)){
        	errorMessage = NOT_A_NUM; 
        	return null;
        }
        int sizeListWordsOpponent = opponentPlayer.getListWord().size(); 
        if(Integer.parseInt(stringNumber) <= 0 || sizeListWordsOpponent < Integer.parseInt(stringNumber)){
        	errorMessage = OUT_OF_RANGE_WORD_OPPONENT; 
        	return null; 
        }
        int idWord = Integer.parseInt(stringNumber); 
        return opponentPlayer.getListWord().get(idWord-1);
	}
	@Override
	public boolean proposeWord() { 
		while(true){
			PrintObject.printHedaer(listPlayer, communPot);
			System.out.println("Tour du joueur => " + name);
	        if(errorMessage != null){
	        	System.out.println(errorMessage);
	        	errorMessage = null; 
	        }
			String word = scan.scan("Composer un nom commun en fonction du pot commun, caractère '*' pour revenir au menu principale:"); 
			if(word.equals("*") ){
				return false;  
			}
			else if(validator.validWord(word, communPot)){
				winProcess(word);
	        	return true; 
			}
			else{
				errorMessage = INVALID_WORD; 
			}
		}	
	}

	@Override
	public boolean stealOpponentWord() {
		while(true){
			PrintObject.printHedaer(listPlayer, communPot);
			if(errorMessage != null){
	        	System.out.println(errorMessage);
	        	errorMessage = null; 
	        }
	        System.out.println("Tour du joueur "+ name + ":");
	        String opponentWord = choseOpponentWord();
	        if(opponentWord == null){
	        	continue; 
	        }else if(opponentWord.equals("*")){
        		return false; 
        	}
	        System.out.println("le mot choisi est '"+ opponentWord +"' :");
	        String word = scan.scan("Proposer un mot en fonction du mot de votre adversaire et du pot commun , '*' pour revenir au menu principal: ");
	        if(word.equals("*") ){
				return false;  
	        }
	        else if(validator.stealWordValidate(word, opponentWord, communPot)){
	        	winProcess(word);
	        	return true; 
	        }
	        else{
	        	errorMessage = INVALID_WORD; 
	        }
		}
	}

	@Override
	public boolean extendWord() {
		while(true){ 
	        PrintObject.printHedaer(listPlayer, communPot);
			System.out.println("Tour du joueur => "+ name + ":");
			if(errorMessage != null){
	        	System.out.println(errorMessage);
	        	errorMessage = null; 
	        }
	        String stringNumber = scan.scan("Choisir un de vos mots en fonction de son numéro :");
	        if(stringNumber.equals("*")){
	        	return false; 
	        }
	        if(!Validator.isNumber(stringNumber)){
	        	errorMessage = NOT_A_NUM; 
	        	continue; 
	        }
	        int sizePlayerWords = listWord.size();
	        if(Integer.parseInt(stringNumber) <= 0 || sizePlayerWords < Integer.parseInt(stringNumber)){
	        	errorMessage = OUT_OF_RANGE_WORD_PLAYER;
	        	continue; 
	        }
	        int indexWord = Integer.parseInt(stringNumber); 
	        String ownWord = listWord.get(indexWord-1); 
	        String yesORno = scan.scan("Voulez vous utiliser un des mots de votre adversaire : [ o / n ] ");
	        String opponentWord = null; 
	        if(yesORno.equals("o")){
	        	opponentWord = choseOpponentWord(); 
	        	if(opponentWord == null){
	        		continue; 
	        	}else if(opponentWord.equals("*")){
	        		return false; 
	        	}
	 	        System.out.println("le mot choisi est '"+ opponentWord +"' :");
	        }
	        String word = scan.scan("Ettendez votre mots, '*' pour revenir au menu principal: ");
	        if(word.equals("*")){
 	        	return false; 
 	        }
	        CharSequence tmpCommunpot = null; 
	        if(opponentWord == null)
	        	tmpCommunpot = communPot; 
	        if(validator.extendWordValidate(word, ownWord, opponentWord, tmpCommunpot)){
	        	winProcess(word);
	        	return true; 
	        }
	        else{
	        	errorMessage = INVALID_WORD; 
	        }
	      }
	}

	@Override
	public void playe() {
		PrintObject.printHedaer(listPlayer, communPot);
		System.out.println("Tour du joueur => " + name);
		if(errorMessageOption != null){
			System.out.println(errorMessageOption);
			errorMessageOption = null; 
		}
			
		String stringNumber = scan.scan("1) Utiliser le pot commu   2) Utiliser un des mots de vos adversaire  3) Ettendre un de vos mots    4) Passer");
		if(!Validator.isNumber(stringNumber)){
			playe();
		}
		else{
			if(Integer.parseInt(stringNumber) < 1 || Integer.parseInt(stringNumber) > 4){
				errorMessageOption = BAD_NUM_OPTION; 
				playe();
			}
			else{
				int indexCommand = Integer.parseInt(stringNumber)- 1;
				Invoker invoker = new Invoker(this);
				if(indexCommand >= listCommands.length)
					return; 
				if(!invoker.getCommandPlayer(listCommands[indexCommand]).execute())
					playe(); 
			}
		}
	}
	
	public void setScan(ScanObject scan) {
		this.scan = scan;
	}
}
